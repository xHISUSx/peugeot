var speed = 500,
	easing = mina.easeinout,
	one, two, three, oneNormal, oneCrashed, twoNormal, twoCrashed, threeNormal, threeCrashed, pathConfig;
$(function(){
	if ($('.wrapper').hasClass('main')) {
		one =  Snap('#one'),
		two = Snap('#two'),
		three = Snap('#three'),
		oneNormal = one.select( '.normal' ),
		oneCrashed = one.select( '.crashed' ),
		twoNormal = two.select( '.normal' ),
		twoCrashed = two.select( '.crashed' ),
		threeNormal = three.select( '.normal' ),
		threeCrashed = three.select( '.crashed' ),
		pathConfig = {
			oneFrom : oneNormal.attr( 'd' ),
			oneTo :  oneCrashed.attr( 'd' ),
			twoFrom : twoNormal.attr( 'd' ),
			twoTo :  twoCrashed.attr( 'd' ),
			threeFrom : threeNormal.attr( 'd' ),
			threeTo :  threeCrashed.attr( 'd' )
		};
	}
});
function crashedNumbers() {
	oneNormal.animate( { 'path' : pathConfig.oneTo }, speed, easing );
	twoNormal.animate( { 'path' : pathConfig.twoTo }, speed, easing );
	threeNormal.animate( { 'path' : pathConfig.threeTo }, speed, easing );
}
function fixNumbers() {
	oneNormal.animate( { 'path' : pathConfig.oneFrom }, speed, easing );
	twoNormal.animate( { 'path' : pathConfig.twoFrom }, speed, easing );
	threeNormal.animate( { 'path' : pathConfig.threeFrom }, speed, easing );
}
