var UI = {
		talentsSection: $("#talents-section"),
		talentsSectionImages: $("#talents-section-images"),
		talentsSectionDesc: $("#talents-section-desc"),
		talentsSectionOverlay: $("#talents-overlay"),
		talentsCarousel: $("#home-carousel")
	},
	IS_MOBILE = true;
$(function(){
	var talentsSection = $("#talents-section");

	if ($('div').hasClass('stories')) {

		setStoriesSectionHeight();

		talentsSection
			.on('mouseenter', '.talent-overlay', function(e) {
				onMouseOverTalent($(this), e);
			});

			$(".stories-wrap").on('mouseleave', function(){

				$("#talents-section-images").children().removeClass("-current");
				$("#talents-section-desc").children().removeClass("-current");
				$("#talents-overlay").children().removeClass("-current");
				$("#talents-section-images").removeClass("-mouseover");
				$("#talents-overlay").removeClass("-mouseover");

			});

		// initMobile();
	}
});
function setStoriesSectionHeight() {
	$('.stories .section').css('height', ($(window).height() - $('.stories .enroll').outerHeight() - 80) + 'px');
}
function onMouseOverTalent($this, e) {
	var index = $this.data("index");
	clearTimeout(UI.talentsSection.timeout);
	UI.talentsSection.timeout = setTimeout(function () {
		var $talents = $("#talents-section-images").children();

		var $talentsSorted = [];
		$talentsSorted.push([$talents[index]]);
		for (var i = 1; i <= 4; i++) {
			var jp = index + i, jn = index - i;
			var c = [];
			if (jp >= 0 && jp < $talents.length)c.push($talents[jp]);
			if (jn >= 0 && jn < $talents.length)c.push($talents[jn]);
			$talentsSorted.push(c)
		}
		_.each($talentsSorted, function (talents, i) {
			_.each(talents, function (talent) {
				var $imagesOfTalent = $(talent).find(".talent-piece-image");
				setTimeout(function () {
					$imagesOfTalent.removeClass("-active");
					$($imagesOfTalent[index]).addClass("-active")
				}, i * 150)
			})
		});
		$("#talents-section-images").children().removeClass("-current");
		$("#talents-section-images").children().eq(index).addClass("-current");
		$("#talents-section-desc").children().removeClass("-current");
		$("#talents-section-desc").children().eq(index).addClass("-current");
		$("#talents-overlay").children().removeClass("-current");
		$("#talents-overlay").children().eq(index).addClass("-current");
		$("#talents-section-images").addClass("-mouseover");
		$("#talents-overlay").addClass("-mouseover")
	}, 400)
};
function initMobile() {

	var percSlide = IS_MOBILE ? 2 / 3 : .6,
		translationoffset = $(window).width() * ((1 - percSlide) / 2),
		homeCarousel = $("#home-carousel"),
		homeCarouselFront = $("#home-carousel-front"),
		homeCarouselBack = $("#home-carousel-back");

	var onResize = function () {
		if ($(window).width() > 970) {
			setStoriesSectionHeight();
		} else if ($(window).width() > 479) {
			var slideWidth = ($(window).width() - 40)* percSlide;
		} else {
			var slideWidth = $(window).width() - 40;
		}

		homeCarousel.find(".talent-slide").css({
			width: slideWidth,
			height: $(window).height() - 40
		})
		homeCarousel.css('height', $(window).height() - 40);
	};
	onResize();
	$(window).on("resize", onResize);
	homeCarouselFront.owlCarousel({
		items: 1,
		loop: true,
		dragEndSpeed: 600,
		autoWidth: true
	});
	homeCarouselBack.owlCarousel({
		items: 1,
		autoWidth: true,
		center: true,
		loop: true,
		dragEndSpeed: 600
	});
	homeCarouselFront.disableRealTimeUpdate = false;

	homeCarouselFront.data("owlCarousel").speed(200);
	homeCarouselBack.data("owlCarousel").speed(200);
	homeCarouselFront.on("drag.owl.carousel", function () {
		homeCarouselFront.addClass("-moving");
		homeCarouselBack.data("owlCarousel").speed(0)
	});
	homeCarouselFront.on("dragged.owl.carousel", function () {
		homeCarouselFront.removeClass("-moving");
		homeCarouselBack.data("owlCarousel").speed(500)
	});
	homeCarouselFront.on("translate.owl.carousel", function (e) {
		if (homeCarouselFront.disableRealTimeUpdate)return;
		homeCarouselBack.data("owlCarousel").animate(e.property.coordinate + translationoffset);
	});

	homeCarousel.on("click", "[data-prev]", function () {
		homeCarouselFront.disableRealTimeUpdate = true;
		setTimeout(function () {
			homeCarouselFront.disableRealTimeUpdate = false
		}, 500);
		homeCarouselBack.data("owlCarousel").prev(501);
		homeCarouselFront.data("owlCarousel").prev(501)
	});

	homeCarousel.on("click", "[data-next]", function () {
		homeCarouselFront.disableRealTimeUpdate = true;
		setTimeout(function () {
			homeCarouselFrontdisableRealTimeUpdate = false
		}, 500);
		homeCarouselBack.data("owlCarousel").next(501);
		homeCarouselFront.data("owlCarousel").next(501)
	})
};